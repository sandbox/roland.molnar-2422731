CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Usage
 * Testing


INTRODUCTION
------------
A payment module for Drupal Commerce, providing the following payment methods:
 * Australian Post SecurePay Direct Post
 * NAB (National Australia Bank) Direct Post v2

As the two methods are almost identical, this module provides a working, tested
method for both.


REQUIREMENTS
------------
 * Drupal Commerce (https://www.drupal.org/project/commerce)


RECOMMENDED MODULES
-------------------

 * Clientside Validation (https://www.drupal.org/project/clientside_validation)

Although Clientside Validation is not a strict requirement, you should enable
it to ensure your payment form is not posted to the bank with wrong or empty
fields. If you don't want to use Clientside Validation, you should implement
your own validation.


INSTALLATION
------------

 1. Enable module

 Optionally:
 2. Download Clientside Validation module
 3. Enable Clientside Validation, Clientside Validation Form and Clientside
 Validation Fapi modules

Note on Clientside Validation

Using Clientside Validation modules need some extra care (that's why we decided
not to set it as dependency), because after you enable them, all of your forms
will have client side validation by default. This is not working in all
situations, so you should review its settings at
/admin/config/validation/clientside_validation/general.

If you only want to use it on the payment form, you can set 'Only validate forms
listed below' and add 'commerce_checkout_form_payment' to the 'Enter form IDs
below' area.


USAGE
-----

You can enable and set up this method as any other payment methods in Drupal
Commerce. See https://drupalcommerce.org/user-guide/payments


TESTING
-------

The module has a sub-module called
commerce_securepayau_direct_post_mock_endpoint.
It provides a simple test interface that emulates the real bank endpoint.
You can use it by setting 'Mode' to 'Local Test' on the payment method rule
configuration.
