/**
 * @file Extends client side validation.
 */

(function ($) {
    // Define a Drupal behaviour with a custom name.
    Drupal.behaviors.commerceSecurePayAUDirectPostAddCreditCardExpiryValidator = {
        attach: function (context) {
            // Add an eventlistener to the document reacting on the
            // 'clientsideValidationAddCustomRules' event.
            $(document).bind('clientsideValidationAddCustomRules', function(event){
                // Add your custom method with the 'addMethod' function of jQuery.validator
                // http://docs.jquery.com/Plugins/Validation/Validator/addMethod#namemethodmessage
                jQuery.validator.addMethod("creditCardExpiry", function(value, element, param) {
                    expiryYear = $(":input[name='EPS_EXPIRYYEAR']").val();
                    expiryMonth = $(":input[name='EPS_EXPIRYMONTH']").val();
                    expiry = expiryYear + '' + expiryMonth;

                    return expiry >= param;

                }, jQuery.format('Field can not be empty'));
            });
        }
    }
})(jQuery);
