<?php

/**
 * @file
 * Simpletest cases for testing commerce_securepayau_direct_post.
 */

/**
 * Class CommerceSecurepayauDirectPostTestCase.
 */
class CommerceSecurepayauDirectPostTestCase extends CommerceBaseTestCase {

  protected $order;

  /**
   * Implementation of getInfo().
   */
  public static function getInfo() {
    return array(
      'name' => 'Commerce Securepay(.com.au) Direct Post',
      'description' => 'Ensure that commerce_securepayau_direct_post module works properly.',
      'group' => 'Drupal Commerce',
    );
  }

  /**
   * Implementation of setUp().
   */
  public function setUp() {
    $modules = parent::setUpHelper('all', array(
      'commerce_securepayau_direct_post',
      'commerce_securepayau_direct_post_mock_endpoint',
    ));
    parent::setUp($modules);

    $this->enableCurrencies(array('AUD'));
    $this->store_admin = $this->createStoreAdmin();
    $this->store_customer = $this->createStoreCustomer();

    // The rule that sends a mail after checkout completion should be disabled
    // as it returns an error caused by how mail messages are stored.
    $rules_config = rules_config_load('commerce_checkout_order_email');
    $rules_config->active = FALSE;
    $rules_config->save();

  }

  /**
   * Test SecurePay AU Direct Post payment method.
   *
   * Test SecurePay AU Direct Post payment method on checkout process using an
   * authenticated user.
   */
  public function testCommerceSecurepayauDirectPostPayment() {

    // Log in as store administrator user.
    $this->drupalLogin($this->store_admin);

    // Go to payment methods page.
    $this->drupalGet('admin/commerce/config/payment-methods');

    // Go to edit payment rule.
    $this->clickLink(t('Securepay AU Direct Post'));
    $this->clickLink(t('Enable payment method: Securepay AU Direct Post'));
    $payment_settings = array(
      'parameter[payment_method][settings][payment_method][settings][payment_reference]' => 'Web Order #[commerce-order:order-id]',
      'parameter[payment_method][settings][payment_method][settings][merchant_id]' => 'ABC123',
      'parameter[payment_method][settings][payment_method][settings][password]' => 'abc123',
      'parameter[payment_method][settings][payment_method][settings][mode]' => 'local_test',
      'parameter[payment_method][settings][payment_method][settings][auto_fill_credit_card]' => 1,
      // @todo: don't use this, fill cc details in place
      'parameter[payment_method][settings][payment_method][settings][currency]' => 'AUD',
    );
    $this->drupalPost(NULL, $payment_settings, t("Save"));

    $this->drupalPost(NULL, array('settings[active]' => 1), t("Save changes"));

    // Log in as normal user.
    $this->drupalLogin($this->store_customer);

    // Order creation, in cart status.
    $this->testProduct = $this->createDummyProduct('MYPRD-01', 'Test Product', 100, 'AUD');
    $this->order = $this->createDummyOrder($this->store_customer->uid, array($this->testProduct->product_id => 1));

    // Access to checkout page.
    $this->drupalGet($this->getCommerceUrl('checkout'));

    // Generate random information, as city, postal code, etc.
    $address_info = $this->generateAddressInformation();

    // Fill in the billing address information.
    $billing_pane = $this->xpath("//select[starts-with(@name, 'customer_profile_billing[commerce_customer_address]')]");
    $this->drupalPostAJAX(NULL, array((string) $billing_pane[0]['name'] => 'US'), (string) $billing_pane[0]['name']);

    // Fill in the required information for billing pane, with a random State.
    $info = array(
      'customer_profile_billing[commerce_customer_address][und][0][name_line]' => $address_info['name_line'],
      'customer_profile_billing[commerce_customer_address][und][0][thoroughfare]' => $address_info['thoroughfare'],
      'customer_profile_billing[commerce_customer_address][und][0][locality]' => $address_info['locality'],
      'customer_profile_billing[commerce_customer_address][und][0][administrative_area]' => 'KY',
      'customer_profile_billing[commerce_customer_address][und][0][postal_code]' => $address_info['postal_code'],
    );
    $this->drupalPost(NULL, $info, t("Continue to next step"));
    $this->drupalPostAJAX(NULL, array('commerce_payment[payment_method]' => 'securepayau_direct_post|commerce_payment_securepayau_direct_post'), 'commerce_payment[payment_method]');

    // Finish checkout process.
    $this->drupalPost(NULL, array(), t("Continue to next step"));

    // @todo: Fill credit card details
    // Process payment.
    $this->drupalPost(NULL, array(), t("Process payment"));

    // Reload the order directly from db to update status.
    $order = commerce_order_load_multiple(array($this->order->order_id), array(), TRUE);

    // Order status should be pending when completing checkout process.
    $this->assertEqual(reset($order)->status, 'pending', t("Order status is 'Pending' after completing checkout"));

    // Check if the completion message has been displayed.
    $this->assertTitle(t("Checkout complete") . ' | Drupal', t("Title of the page is 'Checkout complete' when finishing the checkout process"));
    $this->assertText(t("Your order number is @order-number.", array('@order-number' => $this->order->order_number)), t("Completion message for the checkout is correctly displayed"));

  }

  /*
   * @todo: Tests for different use cases:
   * - Rejected transaction
   * - Invalid values in form (if testing clienside_validation is possible)
   * - Double transaction
   * - Attempt to complete payment with an invalid fingerprint (security)
   */

}
